var fileProcessor = function() {
	var context = {
		baseurl : null,
		file : null,
		tbody : null
	};

	return {
		initiate : function(baseurl, $file, $tbody) {
			context.baseurl = baseurl;
			context.file = $file;
			context.tbody = $tbody;

			context.file
					.fileupload({
						dataType : 'text',
						add : function(e, data) {
							var size = parseInt(data.files[0].size);
							var name = data.files[0].name;

							var $td = $('<td colspan="3"></td>');
							var $tr = $('<tr>');
							$tr.append($td).prependTo(context.tbody);

							// Stop uploading if larger than 1 megabytes
							if (size > 1048576) {
								var text = name + ' is larger than 1 Megabytes';
								$td.html(text);
								var $lblCancel = $(
										'<label style="margin-left: 10px" class="linklabel">')
										.html('Cancel');
								$lblCancel.click(function() {
									$(this).closest('tr').remove();
								}).appendTo($td);

								return;
							}

							// Upload the file
							$td.html('Uploading ' + name + ', ' + size
									+ ' bytes');
							data.context = $td;
							data.submit();
						},
						progress : function(e, data) {
							// The progress data is only for the upload, not
							// including the
							// time to save into the database, so it is not a
							// valid number to show
							// data.context.html(parseInt(data.loaded /
							// data.total * 100, 10)
							// + '% - completed');
						},
						fail : function(e, data) {
							var name = data.files[0].name;
							data.context.html('Failed to upload ' + name);
							var $lblCancel = $(
									'<label style="margin-left: 10px" class="linklabel">')
									.html('Cancel');
							$lblCancel.click(function() {
								$(this).closest('tr').remove();
							});
							data.context.append($lblCancel);

						},
						done : function(e, data) {
							data.context.parent().html(
									$(data.jqXHR.responseText).html());
						},
						// Global events
						progressall : function(e, data) {
						},
						start : function() {
						},
						stop : function() {
						}
					});
		},
		downloadfile : function(id) {
			var url = context.baseurl + '/mvc/api/downloadfile?fileId=' + id;

			var hiddenIFrameID = 'hiddenDownloader';
			var iframe = document.getElementById(hiddenIFrameID);
			if (iframe === null) {
				iframe = document.createElement('iframe');
				iframe.id = hiddenIFrameID;
				iframe.style.display = 'none';
				document.body.appendChild(iframe);
			}

			iframe.contentWindow.location.replace(url);
		},
		deletefile : function(ctl, id) {
			var url = context.baseurl + '/mvc/api/deletefile';

			var ajax = $.ajax({
				cache : false,
				type : "POST",
				data : {
					fileId : id
				},
				url : url
			});

			ajax.done(function() {
				$(ctl).closest('tr').remove();
			}).fail(function() {
				alert('Error occured while deleting the file');
			});
		}
	};
}();
