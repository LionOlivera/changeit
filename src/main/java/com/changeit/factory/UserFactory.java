package com.changeit.factory;

import com.changeit.service.UserService;
import com.changeit.service.Impl.UserServiceHibernateImpl;

public class UserFactory {

	public static UserService getpersistencia(final String persistencia) {
		if (persistencia.equalsIgnoreCase("hibernate")) {
			return new UserServiceHibernateImpl();
		} else if (persistencia.equalsIgnoreCase("spring")) {
			return null;
		} else {
			return null;
		}
	}
}
