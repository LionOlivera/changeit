package com.changeit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "userFotos")
public class UserFotos {

	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "codFoto", columnDefinition = "uniqueidentifier")
	private String codFoto;

	@NotNull
	private int codUser;
	@NotNull
	private String name;
	@NotNull
	private String contentType;
	@NotNull
	private int size;
	@NotEmpty
	private byte[] data;
	@NotEmpty
	private String  UploadTime;

	
	
	
	
	
	public String getCodFoto() {
		return codFoto;
	}

	public void setCodFoto(String codFoto) {
		this.codFoto = codFoto;
	}



	public int getCodUser() {
		return codUser;
	}

	public void setCodUser(int codUser) {
		this.codUser = codUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public String getUploadTime() {
		return UploadTime;
	}

	public void setUploadTime(String uploadTime) {
		UploadTime = uploadTime;
	}

	  public boolean isValidCod() {
		    return codFoto != null ;
		  }
	

}
