package com.changeit.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="clienteMedioPago")
public class UserMedioPago  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7910807164079052883L;

	@Id
	@GeneratedValue
	private Long codClienteMedioPago;
	
	@NotEmpty
	private String tarjeta;
	@NotEmpty
	private String banco; 
	@NotEmpty
	private String numeroTarjeta;
	@NotEmpty
	private String codigoSeguridad;
	
	
	

	public Long getCodClienteMedioPago() {
		return codClienteMedioPago;
	}
	public void setCodClienteMedioPago(Long codClienteMedioPago) {
		this.codClienteMedioPago = codClienteMedioPago;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}
	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}
	
	
	
	
	




	
}
