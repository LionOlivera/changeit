package com.changeit.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="clientePuntos")
public class UserPuntos  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7910807164079052883L;

	@Id
	@GeneratedValue
	private Long codClientePuntos;
	
	@Column(name = "puntos", unique = false, nullable = false)
	private int puntos;
	@NotEmpty
	private String moneda; 
	
	@Column(name = "dinero", unique = false, nullable = false)
	private float dinero;
	
	
	public Long getCodClientePuntos() {
		return codClientePuntos;
	}
	public void setCodClientePuntos(Long codClientePuntos) {
		this.codClientePuntos = codClientePuntos;
	}

	
	public int getPuntos() {
		return puntos;
	}
	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public float getDinero() {
		return dinero;
	}
	public void setDinero(float dinero) {
		this.dinero = dinero;
	}
	
	
	

	



	
}
