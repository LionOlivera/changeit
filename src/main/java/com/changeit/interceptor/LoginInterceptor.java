package com.changeit.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.changeit.model.UserLogin;

public class LoginInterceptor extends HandlerInterceptorAdapter {

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		HttpSession session = request.getSession(false);
		UserLogin user = (UserLogin) session.getAttribute("userLogin");
		boolean flag = user.isValid();
		if (flag == false) {
			System.out.println("Llego nulo");
			response.sendRedirect("/ChangeIT");
			return false;

		} else {
			System.out.println("nombre : " + user.getUserName());
			System.out.println("pass : " + user.getPassword());
		}

		return true;
	}
}