package com.changeit.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;

import com.changeit.model.User;
import com.changeit.persistencia.HibernateUtil;
import com.changeit.service.UserService;

@Service("UserService")
public class UserServiceHibernateImpl implements UserService {
	private static Logger log = Logger.getLogger(UserServiceHibernateImpl.class);

	@Override
	public User save(User user) {

		HibernateUtil.getInstance();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			if (!session.isOpen()) {
				session = HibernateUtil.openSession();
			}
			session.beginTransaction();
			session.save(user);
			session.getTransaction().commit();
			return user;
		} catch (final ConstraintViolationException e) {
			log.warn("Error al tratar de dar de el usuario: " + user.getFirstName() + ", ya fue dado de alta");
			throw new RuntimeException("Error al tratar de dar de el usuario: " + user.getFirstName() + ", ya fue dado de alta");
		} catch (final HibernateException e) {
			log.warn("Erro al dar de alta el usuario" + user.getFirstName() + ": " + e.getMessage());
			throw new RuntimeException("Error al dar de alta el usuario.\nConsulte con el administrador del sistema.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

	}

	@Override
	public boolean delete(int user) {

		// tengo que traer el usuario completo

		// HibernateUtil.getInstance();
		// Session session =
		// HibernateUtil.getSessionFactory().getCurrentSession();
		// try {
		// if (!session.isOpen()) {
		// session = HibernateUtil.openSession();
		// }
		// session.beginTransaction();
		// session.delete(p);
		// session.getTransaction().commit();
		// return true;
		// } catch (final ConstraintViolationException e) {
		// log.warn("Error al tratar de borrar el USUARIO: " + user.getNombre()
		// + ", el mismo ya fue borrado");
		// throw new RuntimeException("Error al tratar de borrar el USUARIO: " +
		// user.getNombre() + ", el mismo ya fue borrado");
		// } catch (final HibernateException e) {
		// log.warn("Error al borrar el USUARIO" + user.getNombre() + ": " +
		// e.getMessage());
		// throw new RuntimeException("Error al borrar el uSUARIO.\nConsulte con
		// el administrador del sistema.");
		// } finally {
		// if (session.isOpen()) {
		// session.close();
		// }
		// }
		return false;

	}

	@Override
	public User edition(User user) {
		HibernateUtil.getInstance();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			if (!session.isOpen()) {
				session = HibernateUtil.openSession();
			}
			session.beginTransaction();
			session.saveOrUpdate(user);
			session.getTransaction().commit();
			return user;
		} catch (final HibernateException e) {
			log.warn("Error al actualiazar el usuario" + user.getFirstName() + ": " + e.getMessage());
			throw new RuntimeException("Error al actualizar el usaurio.\nConsulte con el administrador del sistema.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean findByLogin(String userName, String password) {

		HibernateUtil.getInstance();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		List<User> listaUsuarios = new ArrayList<User>();
		try {
			if (!session.isOpen()) {
				session = HibernateUtil.openSession();
			}
			session.beginTransaction();
			String strQuery = "";
			SQLQuery query;
			strQuery = "SELECT * FROM usuario WHERE userName = '" + userName.trim().toString() + " '";
			System.out.println("Query Update: " + strQuery);
			query = session.createSQLQuery(strQuery);
			query.addEntity(User.class);
			listaUsuarios = query.list();
			session.getTransaction().commit();
			if (listaUsuarios.size() > 0) {
				for (User user : listaUsuarios) {
					if (user != null && user.getPassword().equals(password)) {
						return true;
					}
				}
			} else {
				return false;
			}
			return false;
		} catch (final ConstraintViolationException e) {
			log.warn("Error al buscar un usuario: " + userName + ", el mismo no se encontro");
			throw new RuntimeException("Error al buscar un usuario: " + userName + ", el mismo no se encontro");
		} catch (final HibernateException e) {
			log.warn("Error al buscar un usuario: " + userName + ", el mismo dio este error : -----" + e.getMessage());
			throw new RuntimeException("Error al buscar el usuario.\nConsulte con el administrador del sistema.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

	}

	@Override
	public boolean findByUserName(String userName) {
		HibernateUtil.getInstance();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			if (!session.isOpen()) {
				session = HibernateUtil.openSession();
			}
			session.beginTransaction();
			String strQuery = "";
			SQLQuery query;
			List<User> listaUsuarios = new ArrayList<User>();
			strQuery = "SELECT * FROM usuario WHERE userName = '" + userName.trim().toString() + " '";
			System.out.println("Query Update: " + strQuery);
			query = session.createSQLQuery(strQuery);
			query.addEntity(User.class);
			listaUsuarios = query.list();
			session.getTransaction().commit();
			if (listaUsuarios.size() > 0)
				return true;
			else
				return false;
		} catch (final ConstraintViolationException e) {
			log.warn("Error al buscar un usuario: " + userName + ", el mismo no se encontro");
			throw new RuntimeException("Error al buscar un usuario: " + userName + ", el mismo no se encontro");
		} catch (final HibernateException e) {
			log.warn("Error al buscar un usuario: " + userName + ", el mismo dio este error : -----" + e.getMessage());
			throw new RuntimeException("Error al buscar el usuario.\nConsulte con el administrador del sistema.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

	}

}
