package com.changeit.service;

import com.changeit.model.User;

public interface UserService {
	User save(User user);
	boolean delete (int user);
	User edition (User user);
	boolean findByLogin(String userName, String password);
	boolean findByUserName(String userName);
}
