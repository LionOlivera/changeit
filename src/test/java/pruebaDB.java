
import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.changeit.model.User;
import com.changeit.model.UserMedioPago;
import com.changeit.model.UserPuntos;
import com.changeit.persistencia.HibernateUtil;

public class pruebaDB {

	public static void main(String[] args) {

		UserMedioPago userMedioPago = new UserMedioPago();

		userMedioPago.setBanco("SANTANDER");
		userMedioPago.setCodigoSeguridad("617");
		userMedioPago.setNumeroTarjeta("5312312312");
		userMedioPago.setTarjeta("VISA");
		
		UserPuntos userPuntos = new UserPuntos();
		
		userPuntos.setDinero(Float.valueOf("100"));
		userPuntos.setMoneda("Euro");
		userPuntos.setPuntos(100);
		
		User user = new User ();
		
		Date date = new Date();
		user.setDateOfBirth(date);
		user.setEmailAddress("olivera.lionel@gmail.com");
		user.setUserName("LionOlivera");
		user.setFirstName("Lionel");
		user.setLastName("olivera");
		user.setPassword("1234");
		user.setUserMedioPago(userMedioPago);
		user.setUserPuntos(userPuntos);
		
		
		
		
		
		Session session = HibernateUtil.getInstance().getSessionFactory().getCurrentSession();

		try {
			if (!session.isOpen()) {
				session = HibernateUtil.openSession();
			}
			session.beginTransaction();
			session.flush();
			session.save(user);

			session.getTransaction().commit();

		} catch (final HibernateException e) {
			session.getTransaction().rollback();
			throw new RuntimeException("Error al tratar de dar de alta la Factura y su movimiento contable.\nConsulte con el administrador del sistema.");

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

	}

}
